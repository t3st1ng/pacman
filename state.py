# This is freaking awful, I hate myself rn
from enums import Direction, GameMode
from ghost import Ghost
from level_map import Tiles
from pacman import Pacman
from pampy import match, _
import cocos
import logging

main_scene, score, tiles, pacman, timers, change_mode = [None] * 6
ghosts = {
    'blinky': None,
    'pinky': None,
    'inky': None,
    'clyde': None
}
mode = None
level = 1


def blinky_calc_target(self):
    return pacman.pos_x, pacman.pos_y


def pinky_calc_target(self):
    target = [pacman.pos_x, pacman.pos_y]
    vec = Direction.get_vector(pacman.direction)
    target[0] += vec[0] * 4
    target[1] += vec[1] * 4
    return target


def inky_calc_target(self):
    return 2 * pacman.pos_x - ghosts['blinky'].pos_x, 2 * pacman.pos_y - ghosts['blinky'].pos_y


def clyde_calc_target(self):
    if (self.pos_x - pacman.pos_x) ** 2 + (self.pos_y - pacman.pos_y) ** 2 > 8 ** 2:
        return pacman.pos_x, pacman.pos_y
    else:
        return self.scatter_pos


class ChangeMode:
    paused = False

    def __init__(self, times):
        global main_scene, mode, change_mode
        self.timer = cocos.text.Label(
            ' ',
        )
        main_scene.add(self.timer)
        self.times = list(reversed(times))
        mode = GameMode.scatter
        self.timer.schedule_interval(self.__call__, self.times.pop(), GameMode.chase)

    def __call__(self, time_that_nobody_asked_for, new_mode):
        global mode, main_scene

        if self.paused:
            main_scene.unschedule(self.__call__)
        else:
            self.timer.unschedule(self.__call__)
        logging.debug('Mode changed to %s', new_mode)
        sched_time, sched_mode, self.paused = match((mode, new_mode),
                                                       (_, GameMode.frightened), (6, mode, True),
                                                       (GameMode.frightened, _), (None, None, False),
                                                       (GameMode.scatter, GameMode.chase), lambda x: (self.times.pop(), GameMode.scatter, False),
                                                       (GameMode.chase, GameMode.scatter), lambda x: (self.times.pop(), GameMode.chase, False),
                                                       )
        if self.paused:
            self.paused = True
            self.timer.pause_scheduler()
            if sched_time is not None:
                main_scene.schedule_interval(self.__call__, sched_time, sched_mode)
        else:
            self.timer.resume_scheduler()
            if sched_time is not None:
                self.timer.schedule_interval(self.__call__, sched_time, sched_mode)
        mode = new_mode
        print(mode)


def get_timers(level):
    if level == 1:
        return [7, 20, 7, 20, 5, 20, 5, int(1e9)]
    elif 2 <= level <= 4:
        return [7, 20, 7, 20, 5, 1033, 1 / 60, int(1e9)]
    else:
        return [5, 20, 5, 20, 5, 1037, 1 / 60, int(1e9)]


def get_speeds(level):
    if level == 1:
        return {
            'pacman': {
                GameMode.scatter: 0.8,
                GameMode.chase: 0.8,
                GameMode.frightened: 0.9,
            },
            'ghosts': {
                GameMode.scatter: 0.75,
                GameMode.chase: 0.75,
                GameMode.frightened: 0.5,
            }
        }
    elif level <= 4:
        return {
            'pacman': {
                GameMode.scatter: 0.9,
                GameMode.chase: 0.9,
                GameMode.frightened: 0.95,
            },
            'ghosts': {
                GameMode.scatter: 0.85,
                GameMode.chase: 0.85,
                GameMode.frightened: 0.55,
            }
        }
    elif level <= 20:
        return {
            'pacman': {
                GameMode.scatter: 1,
                GameMode.chase: 1,
                GameMode.frightened: 1,
            },
            'ghosts': {
                GameMode.scatter: 0.95,
                GameMode.chase: 0.95,
                GameMode.frightened: 0.6,
            }
        }
    else:
        return {
            'pacman': {
                GameMode.scatter: 0.9,
                GameMode.chase: 0.9,
                GameMode.frightened: 1,
            },
            'ghosts': {
                GameMode.scatter: 0.95,
                GameMode.chase: 0.95,
                GameMode.frightened: 1,
            }
        }


def next_level():
    global tiles, pacman, ghosts, score, main_scene, level, timers, change_mode
    level += 1
    for obj in main_scene.get_children():
        obj.kill()

    tiles = Tiles()
    main_scene.add(tiles)

    main_scene.add(score)

    speeds = get_speeds(level)

    pacman = Pacman(speeds['pacman'])
    main_scene.add(pacman)

    ghosts = {
        'blinky': Ghost('blinky', (14, 19), (26, 33), True, blinky_calc_target, speeds['ghosts']),
        'pinky': Ghost('pinky', (15, 16), (2, 33), False, pinky_calc_target, speeds['ghosts']),
        'inky': Ghost('inky', (14, 16), (27, -2), False, inky_calc_target, speeds['ghosts']),
        'clyde': Ghost('clyde', (16, 16), (0, -2), False, clyde_calc_target, speeds['ghosts'])
    }

    change_mode = ChangeMode(get_timers(level))

    for ghost in ghosts.values():
        main_scene.add(ghost)


def respawn_ghost(ghost_name):
    global level, score, ghosts, main_scene
    speed = get_speeds(level)['ghosts']
    score.add_score(200)

    if ghost_name == 'blinky':
        ghosts['blinky'] = Ghost('blinky', (14, 19), (26, 33), True, blinky_calc_target, speed)
    elif ghost_name == 'pinky':
        ghosts['pinky'] = Ghost('pinky', (15, 16), (2, 33), False, pinky_calc_target, speed)
    elif ghost_name == 'inky':
        ghosts['inky'] = Ghost('inky', (14, 16), (27, -2), False, inky_calc_target, speed)
    elif ghost_name == 'clyde':
        ghosts['clyde'] = Ghost('clyde', (16, 16), (0, -2), False, clyde_calc_target, speed)

    main_scene.add(ghosts[ghost_name])
