import logging

import cocos
from cocos.director import director

import state
from enums import GameMode
from level_map import WIDTH, HEIGHT
from score import Score

# mixer.init()
# mixer.music.load('resources/music.mp3')
# mixer.music.play()

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

    director.init(width=WIDTH * 2, height=HEIGHT * 2 + 80, autoscale=False, resizable=True)

    state.main_scene = cocos.scene.Scene()
    state.score = Score()

    state.next_level()

    state.main_scene.x += WIDTH
    state.main_scene.y += HEIGHT + 110

    # TODO: change magnification method, rn it smooths picture
    state.main_scene.scale = 2

    director.run(state.main_scene)
