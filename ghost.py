from cocos import sprite

from animations_maps import set_animations_map
from creature import Creature
from enums import Direction, GameMode
from end_game import end_game
from random import choice
import state
import level_map


class Ghost(Creature):
    is_event_handler = True

    def __init__(self, name, pos, scatter_pos, is_outside, calc_target, speed):
        super(Ghost, self).__init__()
        self.speed = speed
        self.images = None
        self.name = name
        self.calc_target = calc_target

        set_animations_map(self)
        self.pos_x, self.pos_y = pos
        self.scatter_pos = scatter_pos
        self.is_outside = is_outside
        self.sp = sprite.Sprite(self.images[Direction.right], (self.pos_x * 8 + 7, self.pos_y * 8 + 4))
        self.dx, self.dy = 0, 0
        self.add(self.sp)

        self.direction = Direction.right
        self.sp.schedule(self.update)

    def calc_direction(self):
        target = self.calc_target(self)
        max_x = len(level_map.map)
        max_y = len(level_map.map[0])

        if state.mode == GameMode.scatter:
            target = self.scatter_pos

        if level_map.map[self.pos_x][self.pos_y] == '_':
            self.is_outside = True

        if self.is_outside:
            forbidden_tiles = ('#', '_')
        else:
            forbidden_tiles = ('#',)
            target = (13, 28)

        if state.mode == GameMode.frightened and self.is_outside:
            directions = []
            for dir in list(Direction):
                vec = Direction.get_vector(dir)
                x, y = (self.pos_x + vec[0]) % max_x, (self.pos_y + vec[1]) % max_y
                if level_map.map[x][y] not in forbidden_tiles and dir != Direction.get_opposite(self.direction):
                    directions.append(dir)
            return choice(directions)

        min_dist, direction = 1e9, Direction.get_opposite(self.direction)
        for dir in list(Direction):
            vec = Direction.get_vector(dir)
            x, y = (self.pos_x + vec[0]) % max_x, (self.pos_y + vec[1]) % max_y
            if level_map.map[x][y] not in forbidden_tiles and dir != Direction.get_opposite(self.direction):
                dist = (x - target[0]) ** 2 + (y - target[1]) ** 2
                if dist < min_dist:
                    min_dist = dist
                    direction = dir
                elif dist == min_dist:
                    if Direction.up in (dir, direction):
                        direction = Direction.up
                    elif Direction.left in (dir, direction):
                        direction = Direction.left
                    elif Direction.right in (dir, direction):
                        direction = direction.right
        return direction

    def update(self, modifiers):
        eps = 1
        if abs(self.dx) < eps and abs(self.dy) < eps:
            #self.dx = 0
            #self.dy = 0

            self.direction = self.calc_direction()
            self.sp.image = self.images[self.direction]

        if state.mode == GameMode.frightened:
            self.sp.image = self.images[GameMode.frightened]
        self.move()

        if abs(self.sp.x - state.pacman.sp.x) + abs(self.sp.y - state.pacman.sp.y) < 8:
            if state.mode == GameMode.frightened:
                self.sp.unschedule(self.update)
                state.respawn_ghost(self.name)
                self.sp.kill()
            else:
                end_game()
