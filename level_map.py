import logging

import cocos
import state

WIDTH = 28 * 8
HEIGHT = 31 * 8

str_map = list(reversed([
    '############################',
    '#............##............#',
    '#.####.#####.##.#####.####.#',
    '#*####.#####.##.#####.####*#',
    '#.####.#####.##.#####.####.#',
    '#..........................#',
    '#.####.##.########.##.####.#',
    '#.####.##.########.##.####.#',
    '#......##....##....##......#',
    '######.##### ## #####.######',
    '     #.##### ## #####.#     ',
    '     #.##          ##.#     ',
    '     #.## ###__### ##.#     ',
    '######.## ###  ### ##.######',
    '      .   #      #   .      ',
    '######.## ######## ##.######',
    '     #.## ######## ##.#     ',
    '     #.##          ##.#     ',
    '     #.## ######## ##.#     ',
    '######.## ######## ##.######',
    '#............##............#',
    '#.####.#####.##.#####.####.#',
    '#.####.#####.##.#####.####.#',
    '#*..##................##..*#',
    '###.##.##.########.##.##.###',
    '###.##.##.########.##.##.###',
    '#......##....##....##......#',
    '#.##########.##.##########.#',
    '#.##########.##.##########.#',
    '#..........................#',
    '############################',
]))

map = None


class Tiles(cocos.layer.Layer):
    def __init__(self):
        global map
        map = [[str_map[i][j] for i in range(len(str_map))] for j in range(len(str_map[0]))]
        super(Tiles, self).__init__()

        map_file = cocos.tiles.load('resources/map.tmx')

        level_map = map_file['map']
        self.food = map_file['food']

        level_map.set_view(0, 0, WIDTH, HEIGHT)
        self.food.set_view(0, 0, WIDTH, HEIGHT)

        self.add(level_map)
        self.add(self.food)

    # TODO: edit ambiguous function name, it doesn't reflect return value
    def remove(self, pos_x, pos_y):
        food = self.food.get_at(pos_x, pos_y)
        is_energizer = False
        if food.get('energizer'):
            logging.debug('Energizer: %s', str(food))
            is_energizer = True

        """
        hack for deleting tmx objects. Deleting them from .objects and then
        calling .set_view, forcing map to rerender
        """
        self.food.objects.remove(food)
        self.food.set_view(0, 0, WIDTH, HEIGHT)

        if len(self.food.objects) == 0:
            state.next_level()

        return is_energizer
