import logging

import pyglet
from cocos import sprite

import level_map
from creature import Creature
from enums import GameMode, Direction
import state


class Pacman(Creature):
    is_event_handler = True

    def __init__(self, speed):
        super(Pacman, self).__init__()

        self.speed = speed
        self.pos_x, self.pos_y = 13, 13
        self.sp = sprite.Sprite(pyglet.resource.animation('resources/pacman.gif'),
                                (self.pos_x * 8 + 7, self.pos_y * 8 + 4), scale=0.04)
        self.dx, self.dy = 3, 0
        self.add(self.sp)
        self.sp.rotation = 180
        self.direction = Direction.left
        self.next_direction = None
        self.coordinate_of_pressed = (2000, 2000)
        self.sp.schedule(self.update)

    def update(self, modifiers):
        if abs(self.coordinate_of_pressed[0] - self.sp.x) + abs(self.coordinate_of_pressed[1] - self.sp.y) <= 3 and \
                self.set_direction(self.next_direction):
            self.dx, self.dy = 0, 0
            self.next_direction = None

        self.move()

        if level_map.map[self.pos_x][self.pos_y] in ['.', '*']:
            level_map.map[self.pos_x][self.pos_y] = ' '
            state.score.add_score(1)
            if state.tiles.remove(self.pos_x * 8 + 1, self.pos_y * 8 + 1):
                state.change_mode(0.5, GameMode.frightened)

    def _is_direction_correct(self, direction):
        if direction == self.direction or direction not in list(Direction):
            return False

        vec = Direction.get_vector(direction)

        return not level_map.map[(self.pos_x + vec[0]) % len(level_map.map)][
                       (self.pos_y + vec[1]) % len(level_map.map[0])] in ('_', '#')

    def set_direction(self, direction):
        if not self._is_direction_correct(direction):
            return False

        self.direction = Direction(direction)
        self.sp.rotation = {
            Direction.left: 180,
            Direction.right: 0,
            Direction.up: 270,
            Direction.down: 90
        }[self.direction]
        return True

    def on_key_press(self, key, modifiers):
        logging.debug('Key_pressed: %s, modifiers: %s', str(key), str(modifiers))

        if key not in list(Direction):
            return

        # TODO Direction
        if key == Direction.get_opposite(self.direction):
            self.next_direction = None
            self.set_direction(Direction(key))
        else:
            self.next_direction = Direction(key)
            self.coordinate_of_pressed = (self.sp.x, self.sp.y)
