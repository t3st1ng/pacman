from enum import Enum, IntEnum, auto


class GameMode(Enum):
    chase = auto()
    scatter = auto()
    frightened = auto()


class Direction(IntEnum):
    left = 65361
    right = 65363
    up = 65362
    down = 65364

    @classmethod
    def get_opposite(cls, direction):
        return {
            Direction.left: Direction.right,
            Direction.right: Direction.left,
            Direction.up: Direction.down,
            Direction.down: Direction.up
        }.get(direction, None)

    @classmethod
    def get_vector(cls, direction):
        return {
            Direction.left: (-1, 0),
            Direction.right: (1, 0),
            Direction.up: (0, 1),
            Direction.down: (0, -1),
        }[direction]
