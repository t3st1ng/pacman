# I have really no idea how to call this file
import cocos

import level_map
from enums import Direction
import state

global_speed = 1.4
eps = 1


class Creature(cocos.layer.Layer):
    is_event_handler = True
    pos_x, pos_y, direction, sp, dx, dy, speed = [None] * 7

    def move(self):
        vec = Direction.get_vector(self.direction)

        if not self.pos_x + vec[0] > 27 and not self.pos_x + vec[0] < 0 and abs(self.dx) < eps and abs(
                self.dy) < eps and \
                level_map.map[self.pos_x + vec[0]][self.pos_y + vec[1]] == '#':
            self.dx = 0
            self.dy = 0
            return

        vec = [i * self.speed[state.mode] * global_speed for i in vec]

        self.dx += vec[0]
        while self.dx <= -5:
            self.pos_x -= 1
            self.dx += 8

        while self.dx >= 4:
            self.pos_x += 1
            self.dx -= 8

        if self.pos_x >= 28:
            self.pos_x -= 28

        if self.pos_x <= -1:
            self.pos_x += 28

        self.dy += vec[1]
        while self.dy <= -5:
            self.pos_y -= 1
            self.dy += 8

        while self.dy >= 4:
            self.pos_y += 1
            self.dy -= 8

        self.sp.x = self.pos_x * 8 + self.dx + 4
        self.sp.y = self.pos_y * 8 + self.dy + 4
