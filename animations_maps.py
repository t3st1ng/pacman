from enums import Direction, GameMode
import pyglet


def set_animations_map(self):
    image = pyglet.image.load('resources/resources.png')

    offset = {
        'blinky': 0,
        'pinky': -16,
        'inky': -32,
        'clyde': -48,
    }[self.name]

    self.images = {
        Direction.right: pyglet.image.Animation.from_image_sequence([
            image.get_region(x=456, y=168 + offset, width=16, height=16),
            image.get_region(x=456 + 16, y=168 + offset, width=16, height=16)], 0.1, True),
        Direction.left: pyglet.image.Animation.from_image_sequence([
            image.get_region(x=456 + 32, y=168 + offset, width=16, height=16),
            image.get_region(x=456 + 32 + 16, y=168 + offset, width=16, height=16)], 0.1, True),
        Direction.up: pyglet.image.Animation.from_image_sequence([
            image.get_region(x=456 + 32 * 2, y=168 + offset, width=16, height=16),
            image.get_region(x=456 + 32 * 2 + 16, y=168 + offset, width=16, height=16)], 0.1, True),
        Direction.down: pyglet.image.Animation.from_image_sequence([
            image.get_region(x=456 + 32 * 3, y=168 + offset, width=16, height=16),
            image.get_region(x=456 + 32 * 3 + 16, y=168 + offset, width=16, height=16)], 0.1, True),
        GameMode.frightened: pyglet.image.Animation.from_image_sequence([
            image.get_region(x=456 + 32 * 4, y=168, width=16, height=16),
            image.get_region(x=456 + 32 * 4 + 16, y=168, width=16, height=16)], 0.1, True),

    }
