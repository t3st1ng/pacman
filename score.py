import cocos
from cocos import text


class Score(cocos.layer.Layer):
    score = 0
    label = None

    def __init__(self):
        super(Score, self).__init__()
        self.label = text.Label("Score: 0", (0, -20), color=(251, 255, 1, 255))
        self.add(self.label)

    def add_score(self, score_points):
        self.remove(self.label)
        self.score += score_points
        self.label = text.Label("Score: " + str(self.score), (0, -20), color=(251, 255, 1, 255))
        self.add(self.label)
